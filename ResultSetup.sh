#########################################################################
#                                                                       #
#       rentalResult Management Script                                  #
#                                                                       #
#       Shell script to manage the rentalResult environment.            #
#                                                                       #
#       1. Linux                                                        #
#       2. Progress OpenEdge                                            #
#       3. Database Operations                                          #
#       4. Java Cluster                                                 #
#       5. Utilities                                                    #
#                                                                       #
#########################################################################

#################################
#                               #
#       Menu Variables          #
#                               #
#################################
CURRENT=show_main_menu

#################################
#                               #
#       Formatting Variables    #
#                               #
#################################
NC='\033[0m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'

#################################
#                               #
#       Script Variables        #
#                               #
#################################
PRODIR=
PROINST=PRO.tar
PROANSWER=response.ini
PROLOG=proinstall.log
DIRCHECK=/usr/oe114
CURRVER=11.4

#################################
#                               #
#       Menu Functions          #
#                               #
#################################
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

show_main_menu(){
  clear
  echo "#################################################"
  echo "#       rentalResult Management Script          #"
  echo "#################################################"
  echo "1. Linux"
  echo "2. Progress OpenEdge"
  echo "3. Database Operations"
  echo "4. Java Cluster"
  echo "5. Utilities"
  echo "6. Exit"

  read option

  case "$option" in
    1) clear ; CURRENT=show_linux ; show_linux;;
    2) clear ; CURRENT=show_progress ; show_progress ;;
    3) clear ; CURRENT=show_database ; show_database ;;
    4) clear ; CURRENT=show_java ; show_java ;;
    5) clear ; CURRENT=show_utilities ; show_utilities ;;
    6) echo "Exiting..." ; exit ;;
    *) $CURRENT ;;
  esac
}

show_linux(){
  clear
  echo "#################################################"
  echo "#       Linux Menu                              #"
  echo "#################################################"
  echo "1. Do Stuff"
  echo "2. Do More Stuff"
  echo "3. Back"

  read option

  case "$option" in
    1) echo "Do Stuff" ; pause ;;
    2) echo "Do More Stuff" ; pause ;;
    3) CURRENT=show_main_menu ;;
    *) echo "Enter a valid option" ; pause ; $CURRENT ;;
  esac

  if [ $? -eq 0 ]; then
    $CURRENT
  fi
}

show_progress(){
  clear
  echo "#################################################"
  echo "#       Progress OpenEdge Menu                  #"
  echo "#################################################"
  echo "1. Validate Progress Installation"
  echo "2. Install Progress"
  echo "3. Back"

  read option

  case "$option" in
    1) validate_progress_install ; pause ;;
    2) install_progress_req ; pause ;;
    3) CURRENT=show_main_menu ;;
    *) echo "Enter a valid option" ; pause ; $CURRENT ;;
  esac

  if [ $? -eq 0 ]; then
    $CURRENT
  fi
}

show_database(){
  clear
  echo "#################################################"
  echo "#       Database Operations Menu                #"
  echo "#################################################"
  echo "1. Check Existing Databases"
  echo "2. Backup Database"
  echo "3. Restore Database"
  echo "4. Upgrade Database to 11.4"
  echo "5. Convert Database to Type II"
  echo "6. Back"

  read option

  case "$option" in
    1) check_existing_dbs ; pause ;;
    2) echo "Backup Database" ; pause ;;
    3) echo "Restore Database" ; pause ;;
    4) echo "Upgrade Database to 11.4" ; pause ;;
    5) echo "Convert Database to Type II" ; pause ;;
    6) CURRENT=show_main_menu ;;
    *) echo "Enter a valid option" ; pause ; $CURRENT ;;
  esac

  if [ $? -eq 0 ]; then
    $CURRENT
  fi
}

show_java(){
  clear
  echo "#################################################"
  echo "#       Java Cluster Menu                       #"
  echo "#################################################"
  echo "1. Check Existing Clusters"
  echo "2. Install Java Cluster"
  echo "3. Update SAP Configuration"
  echo "4. Update Vertex Configuration"
  echo "5. Back"

  read option

  case "$option" in
    1) echo "Check Existing Clusters" ; pause ;;
    2) echo "Install Java Cluster" ; pause ;;
    3) echo "Update SAP Configuration" ; pause ;;
    4) echo "Update Vertex Configuration" ; pause ;;
    5) CURRENT=show_main_menu ;;
    *) echo "Enter a valid option" ; pause ; $CURRENT ;;
  esac

  if [ $? -eq 0 ]; then
    $CURRENT
  fi
}

show_utilities(){
  clear
  echo "#################################################"
  echo "#       Utilities Menu                          #"
  echo "#################################################"
  echo "1. Do Stuff"
  echo "2. Do More Stuff"
  echo "3. Back"

  read option

  case "$option" in
    1) echo "Do Stuff" ; pause ;;
    2) echo "Do More Stuff" ; pause ;;
    3) CURRENT=show_main_menu ;;
    *) echo "Enter a valid option" ; pause ; $CURRENT ;;
  esac

  if [ $? -eq 0 ]; then
    $CURRENT
  fi
}

#########################################
#                                       #
#       Progress OpenEdge Functions     #
#                                       #
#########################################
function validate_progress_install(){
  check_progress_install_log
  check_progress_install_dir
  check_progress_connectivity
  return
}

function check_progress_install_log(){
  if [ -f $PROLOG ]; then
    echo -e "Found $PROLOG...${GREEN}SUCCESS!${NC}"
    if grep -Fxq "ResultCode=0 " $PROLOG; then
      echo -e "Status in $PROLOG...${GREEN}SUCCESS!${NC}"
    else
      echo -e "Status in $PROLOG...${RED}FAILURE!${NC}"
    fi
  else
    echo -e "Found proinstall.log...${RED}FAILURE!${NC}"
  fi
  return
}

function check_progress_install_dir(){
  if [ -d $DIRCHECK ]; then
    echo -e "Found $DIRCHECK...${GREEN}SUCCESS!${NC}"
  else
    echo -e "Found DIRCHECK...${RED}FAILURE!${NC}"
  fi
  return
}

function check_progress_connectivity(){
  if grep $CURRVER "${DIRCHECK}/version"; then
    echo -e "Found version $CURRVER...${GREEN}SUCCESS!${NC}"
  else
    echo -e "Found version $CURRVER...${RED}FAILURE!${NC}"
  fi
  return
}

function install_progress_req(){
  while [ ! -f $proinst ]
  do
    read -p 'Please enter the location of the Progress installation (PRO.tar): ' proinst
    if [ -f $proinst ]; then
      prodir=$(dirname "${proinst}")
      proanswer="${prodir}/$proanswer"
      if [ -f $proanswer ]; then
        echo "Found answer file $proanswer..."
        install_progress
      else
        echo "Unable to locate answer file in same directory as PRO.tar. Exiting..."
        break
      fi
    else
      echo "Progress installtion was not found."
    fi
  done
}

function install_progress(){
  echo "Checking for tmp..."
  if [ ! -d ./tmp ]; then
    echo "tmp does not exist so creating..."
    mkdir tmp
  else
    read -p 'tmp already exists, do you want to remove it[y/n]? ' ans
    if [ $ans = "y" ]; then
      echo "deleting tmp..."
      rm -rf tmp
      echo "creating tmp..."
      mkdir tmp
    else
      echo "Exiting script..."
      return
    fi
  fi
  echo "Extracting Progress installation files to tmp..."
  tar -xf $proinst -C ./tmp
  echo "Kicking off installation..."
  ./tmp/proinst -b $proanswer -l $prolog -n
  if [ -d $dircheck ]; then
    echo "Installation completed sucessfully!"
  else
    echo "Installation failed. Please check $prolog"
  fi
}

#########################################
#                                       #
#       Database Operations Functions   #
#                                       #
#########################################
function check_existing_dbs(){
  declare -a filearray=( $(find / -type f -name "*.db" ! -path "/etc/*" ! -path "/var/*" ! -path "/usr/*") )
  if [ ${#filearray[@]} = 0 ]; then
    echo "No databases located on the server."
    return
  else
    for i in "${filearray[@]}"
    do
      echo "Found database file $i"
      check_db_running $i
      describe_db $i
      list_db_backups $i
      echo
    done
  fi
  return
}

function check_db_running(){
  lkdir=$(dirname "${1}")
  lkfile="${lkdir}/hr.lk"
  if [ -f $lkfile ]; then
    echo -e "${GREEN}RUNNING${NC} as $lkfile"
    check_db_connectivity_online $1
  else
    echo -e "${RED}NOT RUNNING${NC} as $lkfile"
    check_db_connectivity_offline $1
  fi
  return
}

function check_db_connectivity_online(){
  return
}

function check_db_connectivity_offline(){
  return
}

function describe_db(){
  return
}

function list_db_backups(){
  return
}

#################################
#                               #
#       Menu Control Logic      #
#                               #
#################################
trap '' SIGINT SIGQUIT SIGTSTP