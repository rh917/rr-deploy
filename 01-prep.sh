#!/usr/bin/sh
# RHEL 7 post install script
# RJH - Dec 2017

#register system with RHEL (only applicable to redhat)
#echo -e "Please register your system with RedHat"
#echo -e "RedHat Username:"
#read rhuser
#echo -e "RedHat Password:"
#read rhpassword
#sleep 7 
#subscription-manager register --username $rhuser --password $rhpassword --auto-attach
pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}
#disable SELinux if enabled
disable_selinux(){
	setenforce 0	#temporarily disable until reboot
	sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/sysconfig/selinux && cat /etc/sysconfig/selinux        #modify SElinux file to not load on reboot
}


#stop IPtables firewall
disable_firewall(){
	systemctl stop firewalld
	systemctl disable firewalld

}


# install common packages
install_common(){
	yum check-update
	yum install -y nano wget perl git curl nfs-utils bind-utils python python-devel open-vm-tools ntp libstdc++.i686 unzip zip
	yum update -y
}

disable_beta_rpms(){
	subscription-manager repos --disable=rhel-7-server-rt-beta-rpms
}


# Set NTP Name Servers to H&E Equipment Nameservers
set_ntp(){
	sed -i "s/0.rhel.pool.ntp.org/10.5.1.86/g" /etc/ntp.conf
	sed -i "s/1.rhel.pool.ntp.org/10.5.1.87/g" /etc/ntp.conf
	sed -i "s/server 2.rhel.pool.ntp.org iburst/#server 2.rhel.pool.ntp.org iburst/g" /etc/ntp.conf
	sed -i "s/server 3.rhel.pool.ntp.org iburst/#server 3.rhel.pool.ntp.org iburst/g" /etc/ntp.conf
}


#start ntpd
start_ntp(){
	systemctl start ntpd
	systemctl enable ntpd
	cat /etc/ntp.conf | grep iburst
}



#generate SSH key
key_generate(){
	ssh-keygen -t rsa -N "" -f priv.key
}


## Install and register splunk universal forwarder with splunk deployment server
install_splunkfwd(){
	wget -O splunkforwarder-7.0.1-2b5b15c4ee89-linux-2.6-x86_64.rpm 'http://deploy.hees-it.io/pkg/splunk/splunkforwarder-7.0.1-2b5b15c4ee89-linux-2.6-x86_64.rpm'
	rpm -ivh splunkforwarder-7.0.1-2b5b15c4ee89-linux-2.6-x86_64.rpm
	su splunk -c "/opt/splunkforwarder/splunk set deploy-poll spldply01.ad.he-equipment.com:8089"

}


#Call functions:
pause
disable_selinux
disable_firewall
install_common
#disable_beta_rpms
install_splunkfwd




## Begin hostname customizations

#curl http://deploy.hees-it.io/rr/01a-hostname.sh | sh


#Alert complete and restart 30 sec
#echo -e "System will reboot now"
#shutdown -r now
