#!/usr/bin/sh
# RHEL 7 hostname script
# RJH - Oct 2017

echo -e "Please enter your desired hostname (shortname):"
read hostvar
echo -e "Please enter your domain name:"
read domainvar

hostname $hostvar
sed -i "s/localhost.localdomain/$hostvar.$domainvar/g" /etc/hostname && cat /etc/hostname