#!/usr/bin/sh
# RHEL 7 Result Specific customizations
# RJH - Oct 2017


## Create user
echo -e "Desired RResult user account?"
read uaccvar

groupadd Result
useradd $uaccvar -G $uaccvar,Result 


## Set kernel sem limits
echo "1000 102000 512 1000" >/proc/sys/kernel/sem
echo "kernel.sem=1000 102000 512 1000" >>/etc/sysctl.conf



## Set open files limit for RResult user defined above

echo "$uaccvar soft nofile 32000" >>/etc/security/limits.conf
echo "$uaccvar hard nofile 32000" >>/etc/security/limits.conf

## Test appending echo to end of file


## 
## ln to oe scratch

## download RR cluster and Java7
curl -O http://deploy.hees-it.io/rr/files/cluster-secure-0.16.jar
curl -O http://deploy.hees-it.io/rr/files/jdk-7u71-linux-i586.gz

