#!/usr/bin/sh
# RHEL 7 post install script
# RJH - Oct 2017

##register system with RHEL (only applicable to redhat)
#echo -e "Please register your system with RedHat"
#sleep 2
#subscription-manager register --auto-attach

#disable SELinux if enabled
setenforce 0	#temporarily disable until reboot
sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/sysconfig/selinux && cat /etc/sysconfig/selinux        #modify SElinux file to not load on reboot

#stop IPtables firewall
systemctl stop firewalld
systemctl disable firewalld

# install common packages
yum check-update
yum install -y nano wget perl git curl nfs-utils bind-utils python python-devel open-vm-tools ntp libstdc++.i686 unzip zip
yum update -y

# Set NTP Name Servers to H&E Equipment Nameservers
sed -i "s/0.centos.pool.ntp.org/10.5.1.86/g" /etc/ntp.conf
sed -i "s/1.centos.pool.ntp.org/10.5.1.87/g" /etc/ntp.conf
sed -i "s/server 2.centos.pool.ntp.org iburst/#server 2.rhel.pool.ntp.org iburst/g" /etc/ntp.conf
sed -i "s/server 3.centos.pool.ntp.org iburst/#server 3.rhel.pool.ntp.org iburst/g" /etc/ntp.conf

#start ntpd
systemctl start ntpd
systemctl enable ntpd

cat /etc/ntp.conf | grep iburst




#Alert complete and restart 30 sec
#echo -e "System will reboot now"
#shutdown -r now